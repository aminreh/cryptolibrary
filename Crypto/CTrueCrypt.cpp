#include "stdafx.h"
#include "Strsafe.h"
#include <direct.h>
#include "CTrueCrypt.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#define GetCurrentDir _getcwd

// MEMBER DATA.

HANDLE CTrueCrypt::hDriver = INVALID_HANDLE_VALUE;
Password CTrueCrypt::VolumePassword; 
MountOptions CTrueCrypt::mountOptions;

bool CTrueCrypt::isDriverStarted = false;
int CTrueCrypt::mountedDriveLetter = -1;

wstring CTrueCrypt::getPathOfExecutable() {
    WCHAR buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    wstring::size_type pos = wstring(buffer).find_last_of(L"\\/");
    return wstring(buffer).substr(0, pos);
}

void CTrueCrypt::ErrorExit(LPTSTR lpszFunction)
{
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    // Display the error message and exit the process
    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(dw);
}

void CTrueCrypt::StartDeviceDriver()
{
    WCHAR               csPath[MAX_PATH];
    CString             csPathToTrueCryptDriver;
    BOOL                res;
    HANDLE              file;
    WIN32_FIND_DATA     find;
    SC_HANDLE           hServiceControlManager;
    SC_HANDLE           hService;

//  Step 1: Find the driver and try to load the file
    hDriver = CreateFile(WIN32_ROOT_PREFIX, 
        0, 
        0, 
        NULL, 
        OPEN_EXISTING, 
        0, 
        NULL
    );

    if (hDriver == INVALID_HANDLE_VALUE)
    {
        hService = NULL;


        // Current working directory (where application installed to).
        GetWindowsDirectory(csPath, 
            256
        );

        csPathToTrueCryptDriver = csPath;
        csPathToTrueCryptDriver = csPathToTrueCryptDriver.Left(3);

        // Check if the OS is 64 bit
        BOOL isWow64 = FALSE;
        typedef BOOL (__stdcall *LPFN_ISWOW64PROCESS ) (HANDLE hProcess,PBOOL Wow64Process);
        LPFN_ISWOW64PROCESS fnIsWow64Process;
        fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress (GetModuleHandle(L"kernel32"), "IsWow64Process");

        if (fnIsWow64Process != NULL)
            if (!fnIsWow64Process (GetCurrentProcess(), &isWow64))
                isWow64 = FALSE;

        CString csPathToTrueCryptDriver;
        wstring cCurrentPath = getPathOfExecutable();

        if (!isWow64)
        {
            csPathToTrueCryptDriver+="truecrypt.sys";
        }
        else
        {
                csPathToTrueCryptDriver = cCurrentPath.c_str();
                csPathToTrueCryptDriver += L"\\";
                csPathToTrueCryptDriver+="truecrypt-x64.sys";
        }

        file = FindFirstFile(csPathToTrueCryptDriver, 
            &find
        );

        if (file == INVALID_HANDLE_VALUE)
        {
            AfxMessageBox(L"Error: Driver not found!");
            return;
        }

        FindClose(file);

        // Step 2: Open Service Control Manager and create Service
        hServiceControlManager = OpenSCManager(NULL,
            NULL, 
            SC_MANAGER_ALL_ACCESS
        );

        if (hServiceControlManager == NULL)
        {
            if (GetLastError () == ERROR_ACCESS_DENIED)
            {
                AfxMessageBox(L"Program needs admin privelleges");
                return;
            }
        }


        hService = OpenService(hServiceControlManager,
            L"dcrpromntdvr", 
            SERVICE_ALL_ACCESS
        );

        if (hService != NULL)
        {
            // Remove stale service (driver is not loaded but service exists)
            DeleteService(hService);
            CloseServiceHandle(hService);
        }

        hService = CreateService(hServiceControlManager,
            L"dcrpromntdvr", 
            L"dcrpromntdvr",
            SERVICE_ALL_ACCESS, 
            SERVICE_KERNEL_DRIVER, 
            SERVICE_DEMAND_START, 
            SERVICE_ERROR_NORMAL,
            csPathToTrueCryptDriver, 
            NULL, 
            NULL, 
            NULL, 
            NULL, 
            NULL
        );

        if (hService == NULL)
        {
            AfxMessageBox(L"Unable to start service");
            CloseServiceHandle(hServiceControlManager);
            return;
        }

        res = StartService(hService, 
            0, 
            NULL
        );

        ///////////
        // CLEAN //
        ///////////

        DeleteService(hService);

        CloseServiceHandle(hServiceControlManager);
        CloseServiceHandle(hService);

        hDriver = CreateFile(WIN32_ROOT_PREFIX, 
            0, 
            0, 
            NULL, 
            OPEN_EXISTING, 
            0, 
            NULL
        );

        if (hDriver == INVALID_HANDLE_VALUE)
        {
            AfxMessageBox(L"Invalid handle for driver");
            return;
        }
    }


    if (hDriver != INVALID_HANDLE_VALUE)
    {
        DWORD dwResult = 0;
        DWORD inputBufferSize = 256;
        DWORD outputBufferSize = 256;
        DWORD outputBuffer[256];

        BOOL bResult = DeviceIoControl(
            hDriver, 
            DRIVER_VERSION,
            NULL, // input buffer
            inputBufferSize, // input buffer size
            outputBuffer, // output buffer
            outputBufferSize, // output buffer size
            &dwResult, // A pointer to a variable that receives the size of the data stored in the output buffer, in bytes
            NULL
        );

        if (bResult == FALSE)
        {
            ErrorExit(TEXT("DeviceIoControl"));
        }

        isDriverStarted = true;
    }
}

void CTrueCrypt::StopDeviceDriver()
{
    /////////////////////
    // LOCAL VARIABLES //
    /////////////////////

    int                                     refDevDeleted;
    DWORD                                   dwResult;
    int                                     x;
    BOOL                                    bResult;
    BOOL                                    bRet;
    MOUNT_LIST_STRUCT                       driver;
    SERVICE_STATUS                          status;
    SC_HANDLE                               hServiceControlManager;
    SC_HANDLE                               hService;

    ///////////
    // CLEAN //
    ///////////

    // Close the device driver handle.
    if (hDriver != INVALID_HANDLE_VALUE)
    {
        // If a dismount was forced in the lifetime of the driver, Windows may later prevent it
        // to be loaded again from the same path. Therefore, the driver will not be 
        // unloaded even though it was loaded in non-install mode.
        if (!DeviceIoControl(hDriver, REFERENCED_DEV_DELETED, NULL, 0, &refDevDeleted, sizeof (refDevDeleted), &dwResult, NULL))
        {
            refDevDeleted = 0;
        }


        if (!refDevDeleted)
        {
            ///////////////////
            // UNLOAD DRIVER //
            ///////////////////

            hService = NULL;


            //if (hDriver == INVALID_HANDLE_VALUE)
            //{
            //	return TRUE;
            //}


            // Test for mounted volumes.
            bResult = DeviceIoControl(hDriver, 
                MOUNT_LIST_ALL, 
                &driver, 
                sizeof(driver), 
                &driver,
                sizeof(driver), 
                &dwResult, 
                NULL
            );


            if (bResult)
            {
                if (driver.ulMountedDrives != 0)
                {
                    //return FALSE;
                }
            }
            else
            {
                //return TRUE;
            }

            CloseHandle(hDriver);
            hDriver = INVALID_HANDLE_VALUE;


            // Stop driver service.
            hServiceControlManager = OpenSCManager(NULL,
                NULL, 
                SC_MANAGER_ALL_ACCESS
            );


            hService = OpenService(hServiceControlManager,
                L"dcrpromntdvr", 
                SERVICE_ALL_ACCESS
            );


            bRet = QueryServiceStatus(hService, 
                &status
            );


            if (status.dwCurrentState != SERVICE_STOPPED)
            {
                ControlService(hService, 
                    SERVICE_CONTROL_STOP, 
                    &status
                );


                for (x = 0; x < 5; x++)
                {
                    bRet = QueryServiceStatus(hService, 
                        &status
                    );


                    if (status.dwCurrentState == SERVICE_STOPPED)
                    {
                        break;
                    }
                }
            }




            if (hService != NULL)
                CloseServiceHandle (hService);

            if (hServiceControlManager != NULL)
                CloseServiceHandle (hServiceControlManager);

            if (status.dwCurrentState == SERVICE_STOPPED)
            {
                hDriver = INVALID_HANDLE_VALUE;
                //return TRUE;
            }

        }
        else
        {
            CloseHandle (hDriver);
        }

        isDriverStarted = false;
    }
}

void CTrueCrypt::BroadcastDeviceChange(WPARAM message, 
    int nDosDriveNo, 
    DWORD driveMap
)
{
    DEV_BROADCAST_VOLUME dbv;
    DWORD dwResult;
    LONG eventId = 0;
    int i;

    if (message == DBT_DEVICEARRIVAL)
        eventId = SHCNE_DRIVEADD;
    else if (message == DBT_DEVICEREMOVECOMPLETE)
        eventId = SHCNE_DRIVEREMOVED;

    if (driveMap == 0)
        driveMap = (1 << nDosDriveNo);

    if (eventId != 0)
    {
        for (i = 0; i < 26; i++)
        {
            if (driveMap & (1 << i))
            {
                char root[] = {i + 'A', ':', '\\', 0 };
                SHChangeNotify (eventId, SHCNF_PATH, root, NULL);
            }
        }
    }

    dbv.dbcv_size = sizeof (dbv); 
    dbv.dbcv_devicetype = DBT_DEVTYP_VOLUME; 
    dbv.dbcv_reserved = 0;
    dbv.dbcv_unitmask = driveMap;
    dbv.dbcv_flags = 0; 

    SendMessageTimeout (HWND_BROADCAST, WM_DEVICECHANGE, message, (LPARAM)(&dbv), 0, 1000, &dwResult);
}

void CTrueCrypt::CStringToCharPtr(CString inputString, char* inputCharBuffer)
{
    wcstombs(inputCharBuffer, inputString.GetBuffer(), inputString.GetLength());
    inputCharBuffer[inputString.GetLength()] = '\0';

    inputString.ReleaseBuffer();

}

int CTrueCrypt::Mount(int iDrive, char* passwordString, int passwordLength, char* pathToContainerArg)
{
    /////////////////////
    // LOCAL VARIABLES //
    /////////////////////

    int										k;
    LPWSTR									lpszNewText;
    MOUNT_STRUCT							mount;
    DWORD									dwResult;
    BOOL									bResult;
    BOOL									cachePassword;
    BOOL									sharedAccess;
    BOOL									quiet;
    BOOL									bReportWrongPassword;
    WCHAR									root[MAX_PATH];

    /* Previous input of this function
    int iDrive , csPathToServicePack, csPassword , now hardcoded
    */

    // TODO: Clean this up, probably both vars not needed ? 
    mountedDriveLetter = iDrive;

    strcpy_s((char*)VolumePassword.Text,
        (passwordLength + 1), passwordString);

    VolumePassword.Length = passwordLength;

    cachePassword = NULL;
    sharedAccess = NULL;
    quiet = FALSE;
    bReportWrongPassword = TRUE;
    Password* password = &VolumePassword;

    ZeroMemory(&mount, 
        sizeof(mount)
    );

    mount.bExclusiveAccess = sharedAccess ? FALSE : TRUE;

    mount.nDosDriveNo = iDrive;
    mount.bCache = cachePassword;

    if (password != NULL)
        mount.VolumePassword = *password;
    else
        mount.VolumePassword.Length = 0;


    if (!mountOptions.ReadOnly && mountOptions.ProtectHiddenVolume)
    {
        mount.ProtectedHidVolPassword = mountOptions.ProtectedHidVolPassword;
        mount.bProtectHiddenVolume = TRUE;
    }
    else
        mount.bProtectHiddenVolume = FALSE;


    mount.bMountReadOnly = mountOptions.ReadOnly;
    mount.bMountRemovable = mountOptions.Removable;
    mount.bSystemVolume = mountOptions.SystemVolume;
    mount.bPersistentVolume = mountOptions.PersistentVolume;
    mount.bPreserveTimestamp = mountOptions.PreserveTimestamp;
    mount.bMountManager = TRUE;

    // Get path of container

#if 0
    ofstream myfile;
    myfile.open("example.txt");
    myfile << "Writing this to a file " << endl;
    myfile << "pathToContainerArg: " << pathToContainerArg << endl;
    myfile << "strlen(pathToContainerArg): " << strlen(pathToContainerArg) << endl;
    myfile.close();
#endif

    strcpy_s((char*)mount.wszVolume,
        (strlen(pathToContainerArg)+1),
        (char*)pathToContainerArg
    );

    //std::wstring pathToContainerStd(pathToContainerArg);
    std::wstring pathToContainerStd(pathToContainerArg, pathToContainerArg + strlen(pathToContainerArg));

    ///////////////////////
    // GetVolumePathName //
    ///////////////////////

    typedef BOOL (WINAPI* tGetVolumePathNameA)(LPCTSTR, LPTSTR, DWORD);

    tGetVolumePathNameA pGetVolumePathNameA = 0;

    HINSTANCE handle = ::LoadLibrary(L"Kernel32.dll");

    if (handle)
    {
        pGetVolumePathNameA = (tGetVolumePathNameA)::GetProcAddress(handle, "GetVolumePathNameA");
    }

    if (pGetVolumePathNameA)
    {
        WCHAR szValumePath[MAX_PATH] = {0};

        pGetVolumePathNameA(L".",
            szValumePath,
            sizeof(szValumePath)
        );
    }

    if (handle)
    {
        ::FreeLibrary(handle);
    }

    if (pGetVolumePathNameA(pathToContainerStd.c_str(), root, sizeof (root) - 1))
    {
        DWORD bps, flags, d;
        if (GetDiskFreeSpace (root, &d, &bps, &d, &d))
            mount.BytesPerSector = bps;

        // Read-only host filesystem
        if (!mount.bMountReadOnly && GetVolumeInformation (root, NULL, 0,  NULL, &d, &flags, NULL, 0))
            mount.bMountReadOnly = (flags & FILE_READ_ONLY_VOLUME) != 0;

        // Network drive
        if (GetDriveType (root) == DRIVE_REMOTE)
            mount.bUserContext = TRUE;
    }


    /////////////
    // UNICODE //
    /////////////
    // converts a SBCS string to a UNICODE string.
    // Not too sure what the following bit does...
    int j = (int) strlen ((char*) mount.wszVolume);
    if (j == 0)
    {
        wcscpy ((LPWSTR) (char*) mount.wszVolume, (LPWSTR) WIDE (""));
    }
    else
    {

        k = (j + 1) * 2;

        void* z = (void*) TCalloc (k);

        if (z)
        {
            lpszNewText = (LPWSTR) z;
        }
        else
        {
            AfxMessageBox(L"OUT OF MEMORY");
            return 1;
        }


        j = MultiByteToWideChar(CP_ACP, 0L, (char*) mount.wszVolume, -1, lpszNewText, j + 1);

        if (j > 0)
        {
            wcscpy((LPWSTR) (char*) mount.wszVolume, 
                lpszNewText
            );
        }
        else
        {
            wcscpy((LPWSTR) (char*) mount.wszVolume, 
                (LPWSTR) ""
            );
        }


        free (lpszNewText);
    }
    // Let go the device code.
    // A very large output buffer to make he control code happy

    const DWORD outputBufferSize = (MAX_PATH*5);
    DWORD outputBuffer[outputBufferSize];
    DWORD inputBufferSize = sizeof(mount);

    bResult = DeviceIoControl(CTrueCrypt::hDriver, 
        MOUNT, 
        &mount, // input buffer
        sizeof(mount), // input buffer size
        outputBuffer, // output buffer
        outputBufferSize, // output buffer size
        &dwResult, 
        NULL
    );

    if (bResult == FALSE)
    {
        ErrorExit(L"DeviceIoControl");

        // Volume already open by another process
        if (GetLastError () == ERROR_SHARING_VIOLATION)
        {
            if (mount.bExclusiveAccess == FALSE)
            {
                if (!quiet)
                    AfxMessageBox(L"FILE IN USE FAILED");

                return -1;
            }
            else
            {
                if (quiet)
                {
                    mount.bExclusiveAccess = FALSE;
                    

                    return 0;
                }

                // Ask user 
                if (IDYES == MessageBoxW ( NULL,  (LPCWSTR) "FILE_IN_USE",
                NULL, MB_YESNO | MB_DEFBUTTON2 | MB_ICONEXCLAMATION))
                {
                    mount.bExclusiveAccess = FALSE;
                    

                    return 0;
                }
            }

            return -1;
        }

        // Mount failed in kernel space => retry in user process context
        if (!mount.bUserContext)
        {
            mount.bUserContext = TRUE;

            return 0;
        }

        return -1;
    }

    if (mount.nReturnCode != 0)
    {
        if (mount.nReturnCode == ERR_PASSWORD_WRONG)
        {

            //// Do not report wrong password, if not instructed to 
            //if (bReportWrongPassword)
            //	CTrueCrypt::handleError(mount.nReturnCode);

            return 0;
        }

        return 0;
    }

    CTrueCrypt::BroadcastDeviceChange(DBT_DEVICEARRIVAL, iDrive, 0);
    if (mount.bExclusiveAccess == FALSE)
        return 2;

    return 1;
}

int CTrueCrypt::Dismount()
{
    /////////////////////
    // LOCAL VARIABLES //
    /////////////////////

    int										status;

    int										result;
    int										dismountMaxRetries;

    BOOL									forced;

    int iDrive = mountedDriveLetter;

    ////////////////////
    // UNMOUNT VOLUME //
    ////////////////////

    dismountMaxRetries = UNMOUNT_MAX_AUTO_RETRIES;
    // Forcing unmount
    forced = true;


    BroadcastDeviceChange(DBT_DEVICEREMOVEPENDING, 
        iDrive, 
        0
    );


    do
    {
        ///////////////////////////
        // DRIVER UNMOUNT VOLUME //
        ///////////////////////////

        UNMOUNT_STRUCT unmount;
        DWORD dwResult;

        BOOL bResult;
        
        unmount.nDosDriveNo = iDrive;
        unmount.ignoreOpenFiles = forced;

        bResult = DeviceIoControl(hDriver, 
            UNMOUNT, 
            &unmount,
            sizeof(unmount), 
            &unmount, 
            sizeof(unmount),
            &dwResult, 
            NULL
        );

        if (bResult == FALSE)
        {

            ErrorExit(L"Dismount: DeviceIoControl:");

            status = 1;
        }

        result = unmount.nReturnCode;


        if (result == ERR_FILES_OPEN)
        {
            Sleep(UNMOUNT_AUTO_RETRY_DELAY);
        }
        else
        {
            break;
        }

    } while (--dismountMaxRetries > 0);


    if (result != 0)
    {
        if (result == ERR_FILES_OPEN)
        {
            //AfxMessageBox(L"UNMOUNT LOCK FAILED");

            status = FALSE;
        }

        //AfxMessageBox(L"UNMOUNT_FAILED");

        status = FALSE;
    } 
    
    BroadcastDeviceChange (DBT_DEVICEREMOVECOMPLETE, iDrive, 0);

    status = TRUE;

    return status;
}

int CTrueCrypt::DismountAll()
{
    //HWND hwndDlg;
    BOOL forceUnmount;
    BOOL interact;
    int dismountMaxRetries;
    int dismountAutoRetryDelay;


    BOOL status = TRUE;
    MOUNT_LIST_STRUCT mountList;
    DWORD dwResult;
    UNMOUNT_STRUCT unmount;
    BOOL bResult;
    unsigned __int32 prevMountedDrives = 0;
    int i;


    forceUnmount = 0;
    interact = 1;
    dismountMaxRetries = 10;
    dismountAutoRetryDelay = 50;



retry:
    //WaitCursor();

    DeviceIoControl (hDriver, MOUNT_LIST, &mountList, sizeof (mountList), &mountList, sizeof (mountList), &dwResult, NULL);

    if (mountList.ulMountedDrives == 0)
    {
        //NormalCursor();
        return TRUE;
    }

    BroadcastDeviceChange (DBT_DEVICEREMOVEPENDING, 0, mountList.ulMountedDrives);

    prevMountedDrives = mountList.ulMountedDrives;

    for (i = 0; i < 26; i++)
    {
        if (mountList.ulMountedDrives & (1 << i))
        {
            //if (bCloseDismountedWindows)
            //	CloseVolumeExplorerWindows (hwndDlg, i);
        }
    }

    unmount.nDosDriveNo = 0;
    unmount.ignoreOpenFiles = forceUnmount;

    do
    {
        bResult = DeviceIoControl (hDriver, UNMOUNT_ALL, &unmount,
            sizeof (unmount), &unmount, sizeof (unmount), &dwResult, NULL);

        if (bResult == FALSE)
        {
            //NormalCursor();

            ////////////////////////////////////////////////////////
            //handleWin32Error (hwndDlg);


            PWSTR lpMsgBuf;
            DWORD dwError = GetLastError ();

            //if (Silent || dwError == 0)
            //	return dwError;

            // Access denied
            if (dwError == ERROR_ACCESS_DENIED /*&& !IsAdmin ()*/)
            {
                //Error ("ERR_ACCESS_DENIED");
                return dwError;
            }

            FormatMessageW (
                FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                        NULL,
                        dwError,
                        MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),	/* Default language */
                        (PWSTR) &lpMsgBuf,
                        0,
                        NULL
                );

            //MessageBoxW (hwndDlg, lpMsgBuf, lpszTitle, ICON_HAND);
            LocalFree (lpMsgBuf);

            // Device not ready
            if (dwError == ERROR_NOT_READY)
                //CheckSystemAutoMount();

            //return dwError;
            ////////////////////////////////////////////////////////

            return FALSE;
        }

        if (unmount.nReturnCode == ERR_FILES_OPEN)
            Sleep (dismountAutoRetryDelay);
        else
            break;

    } while (--dismountMaxRetries > 0);

    memset (&mountList, 0, sizeof (mountList));
    DeviceIoControl (hDriver, MOUNT_LIST, &mountList, sizeof (mountList), &mountList, sizeof (mountList), &dwResult, NULL);
    BroadcastDeviceChange (DBT_DEVICEREMOVECOMPLETE, 0, prevMountedDrives & ~mountList.ulMountedDrives);

    //RefreshMainDlg (hwndDlg);

    //if (nCurrentOS == WIN_2000 && RemoteSession && !IsAdmin ())
    //	LoadDriveLetters (GetDlgItem (hwndDlg, IDC_DRIVELIST), 0);

    //NormalCursor();

    if (unmount.nReturnCode != 0)
    {
        if (forceUnmount)
            status = FALSE;

        if (unmount.nReturnCode == ERR_FILES_OPEN)
        {
            //if (interact && IDYES == AskWarnNoYes ("UNMOUNTALL_LOCK_FAILED"))
            if (interact && IDYES == AfxMessageBox(L"UNMOUNTALL_LOCK_FAILED"))
            {
                forceUnmount = TRUE;
                goto retry;
            }

            return FALSE;
        }
        
        if (interact)
            //MessageBoxW (hwndDlg, GetString ("UNMOUNT_FAILED"), lpszTitle, MB_ICONERROR);
            AfxMessageBox(L"UNMOUNT_FAILED");
    }
    //else
    //{
    //	if (bBeep)
    //		MessageBeep (-1);
    //}

    return status;
}



//int CTrueCrypt::DismountAll()
//{
//	/////////////////////
//	// LOCAL VARIABLES //
//	/////////////////////
//
//
//	BOOL									forceUnmount; 
//	BOOL									interact;
//	
//	int										dismountMaxRetries;
//	int										dismountAutoRetryDelay;
//
//	unsigned __int32						prevMountedDrives;
//
//	BOOL									status;
//	BOOL									bResult;
//
//	DWORD									dwResult;
//
//	MOUNT_LIST_STRUCT						mountList;
//
//	UNMOUNT_STRUCT							unmount;
//
//
//
//
//
//	forceUnmount = 0; 
//	interact = TRUE;
//	
//	dismountMaxRetries = 5;
//	dismountAutoRetryDelay = 50;
//
//	prevMountedDrives = 0;
//	status = TRUE;
//
//
//	DeviceIoControl(hDriver, 
//		MOUNT_LIST, 
//		&mountList, 
//		sizeof (mountList), 
//		&mountList, 
//		sizeof (mountList), 
//		&dwResult, 
//		NULL
//	);
//
//	if (mountList.ulMountedDrives == 0)
//	{
//		return TRUE;
//	}
//
//	BroadcastDeviceChange(DBT_DEVICEREMOVEPENDING, 
//		0, 
//		mountList.ulMountedDrives
//	);
//
//	prevMountedDrives = mountList.ulMountedDrives;
//
//
//
//	unmount.nDosDriveNo = 0;
//	unmount.ignoreOpenFiles = forceUnmount;
//
//	do
//	{
//		bResult = DeviceIoControl(hDriver, 
//			UNMOUNT_ALL, 
//			&unmount,
//			sizeof (unmount), 
//			&unmount, 
//			sizeof (unmount), 
//			&dwResult, 
//			NULL
//		);
//
//		if (bResult == FALSE)
//		{
//			//handleWin32Error();
//			return FALSE;
//		}
//
//		if (unmount.nReturnCode == ERR_FILES_OPEN)
//		{
//			Sleep (dismountAutoRetryDelay);
//		}
//		else
//			break;
//
//	} while (--dismountMaxRetries > 0);
//
//
//	memset(&mountList, 
//		0, 
//		sizeof (mountList)
//	);
//
//	DeviceIoControl(hDriver, 
//		MOUNT_LIST, 
//		&mountList, 
//		sizeof (mountList), 
//		&mountList, 
//		sizeof (mountList), 
//		&dwResult, 
//		NULL
//	);
//
//	BroadcastDeviceChange(DBT_DEVICEREMOVECOMPLETE, 
//		0, 
//		prevMountedDrives & ~mountList.ulMountedDrives
//	);
//
//
//
//	if (unmount.nReturnCode != 0)
//	{
//		if (forceUnmount)
//			status = FALSE;
//
//		if (unmount.nReturnCode == ERR_FILES_OPEN)
//		{
//			if (interact && IDYES == AfxMessageBox("UNMOUNTALL LOCK FAILED"))
//			{
//				forceUnmount = TRUE;
//				return FALSE;
//			}
//
//			return FALSE;
//		}		
//	}
//
//
//	return status;
//}
