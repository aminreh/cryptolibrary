#include "stdafx.h"
#include <stdio.h>
#include "CTrueCrypt.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//#define LOGGING_ENABLED

extern "C"
{
    __declspec(dllexport) void DisplayHelloFromDLL()
    {
        AfxMessageBox(L"Hello from DLL");
    }


    /* Helper method to start driver*/
    __declspec(dllexport) void startDeviceDriver()
    {
        if (CTrueCrypt::isDriverStarted == false) {
            CTrueCrypt::StartDeviceDriver();
        }
    }

    /* Helper method to stop driver*/
    __declspec(dllexport) void stopDeviceDriver()
    {
        if (CTrueCrypt::isDriverStarted == true) {
            CTrueCrypt::StopDeviceDriver();
        }
    }

    /* Helper method to mount container*/
    __declspec(dllexport) void mountContainer(int iDrive, char* password,int passwordLength, char* pathToContainer)
    {
        if (CTrueCrypt::isDriverStarted == true) {
            CTrueCrypt::Mount(iDrive, password, passwordLength, pathToContainer);
        }
    }

    /* Helper method to unmount container*/
    __declspec(dllexport) void unMountContainer()
    {
        if (CTrueCrypt::isDriverStarted == true) {
            CTrueCrypt::Dismount();
        }
    }
}