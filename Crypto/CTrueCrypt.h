#ifndef CTrueCrypt_h
#define CTrueCrypt_h

#include <dbt.h>
#include <winsvc.h>
#include <iostream>
using namespace std;


#define VERSION_NUM							0x043a // Version number to compare against driver
#define UNMOUNT								466956	/* Unmount a volume */
#define MOUNT_LIST							466948	/* Return list of mounted volumes */
#define UNMOUNT_ALL							475112	/* Unmount all volumes */
#define ERR_FILES_OPEN						7
#define UNMOUNT_MAX_AUTO_RETRIES			5
#define UNMOUNT_AUTO_RETRY_DELAY			50
#define DRIVER_VERSION						466968	/* Current driver version */
#define MOUNT_LIST_ALL						466948	/* Return list of mounted volumes */
#define WIN32_ROOT_PREFIX					DRIVER_STR(L"\\\\.\\TrueCrypt")
#define MAX_PASSWORD						64		// Maximum password length
#define WIDE(x)								(LPWSTR)L##x
#define TCalloc								malloc
#define ERR_PASSWORD_WRONG					3
#define MOUNT								466944	/* Mount a volume or partition */
#define REFERENCED_DEV_DELETED				467012

#ifdef NT4_DRIVER
#define DRIVER_STR WIDE
#else
#define DRIVER_STR
#endif


// GLOBALS.

typedef struct
{
	int nDosDriveNo;	/* Drive letter to unmount */
	BOOL ignoreOpenFiles;
	int nReturnCode;	/* Return code back from driver */
} UNMOUNT_STRUCT;

typedef struct
{
	unsigned __int32 ulMountedDrives;	/* Bitfield of all mounted drive letters */
	short wszVolume[26][ MAX_PATH];	/* Volume names of mounted volumes */
	unsigned __int64 diskLength[26];
	int ea[26];
	int volumeType[26];	/* Volume type (e.g. PROP_VOL_TYPE_OUTER, PROP_VOL_TYPE_OUTER_VOL_WRITE_PREVENTED, etc.) */
} MOUNT_LIST_STRUCT;

typedef struct
{
	int Length;
	unsigned char Text[MAX_PASSWORD + 1];
} Password;

typedef struct
{
	BOOL ReadOnly;
	BOOL Removable;
	BOOL ProtectHiddenVolume;
	BOOL PreserveTimestamp;
	BOOL SystemVolume;
	BOOL PersistentVolume;
	Password ProtectedHidVolPassword;	/* Password of hidden volume to protect against overwriting */
} MountOptions;

typedef struct
{
	int nReturnCode;					/* Return code back from driver */
	//short wszVolume[MAX_PATH];			/* Volume to be mounted */
    short wszVolume[MAX_PATH];			/* Volume to be mounted */
	Password VolumePassword;			/* User password */
	BOOL bCache;						/* Cache passwords in driver */
	int nDosDriveNo;					/* Drive number to mount */
	int BytesPerSector;
	BOOL bSystemVolume;					/* Volume is used by system and hidden from user */
	BOOL bPersistentVolume;				/* Volume is hidden from user */
	BOOL bMountReadOnly;				/* Mount volume in read-only mode */
	BOOL bMountRemovable;				/* Mount volume as removable media */
	BOOL bExclusiveAccess;				/* Open host file/device in exclusive access mode */
	BOOL bMountManager;					/* Announce volume to mount manager */
	BOOL bUserContext;					/* Mount volume in user process context */
	BOOL bPreserveTimestamp;			/* Preserve file container timestamp */
	// Hidden volume protection
	BOOL bProtectHiddenVolume;			/* TRUE if the user wants the hidden volume within this volume to be protected against being overwritten (damaged) */
	Password ProtectedHidVolPassword;	/* Password to the hidden volume to be protected against overwriting */
} MOUNT_STRUCT;



// CLASS.

class CTrueCrypt
{
	public:
		// MEMBER DATA.

		static HANDLE						hDriver;

		static Password						VolumePassword; 

		static MountOptions					mountOptions;

        static bool                         isDriverStarted;
        static int                          mountedDriveLetter;

	public:
		// MEMBER METHODS.

		// PROCEDURES.

		static void StartDeviceDriver();
		static void StopDeviceDriver();

		static void BroadcastDeviceChange(WPARAM message, 
			int nDosDriveNo, 
			DWORD driveMap
		);

        // Mount the drive, input: iDrive: the int which will be the Drive number available to mount
		static int Mount(int iDrive, char* password, int passwordLength, char* pathToContainer);

		static int Dismount();
		static int DismountAll();

private:
        // Retrive error message for the last error code
        static void ErrorExit(LPTSTR lpszFunction);
        // Retrieve the path of the running executable and return as a wstring
        static wstring getPathOfExecutable();
        // Utility method to return char* from CString
        static void CStringToCharPtr(CString inputString, char* passwordBuffer);

};

#endif